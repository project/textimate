/**
 * @file
 * Contains definition of the behaviour textimate initial.
 */

(function ($, Drupal, drupalSettings, once) {
  "use strict";

  Drupal.behaviors.textimateInit = {
    attach: function (context, settings) {

      const compat = drupalSettings.textimate.compat;
      const animations = drupalSettings.textimate.animations;

      $.each(animations, function (index, animation) {
        let options = {
          selector: animation.selector,
          autoStart: animation.autoStart,
          loop: animation.loop,
          minDisplayTime: animation.minDisplayTime,
          initialDelay: animation.initialDelay,
          effects: animation.effects,
          type: animation.split,
          event: animation.event,
          compat: compat,
        };

        if (Array.isArray(animation.selector)) {
          $.each(animation.selector, function (index, selector) {
            if (once('textimate', selector).length) {
              options.selector = selector;
              options.effects = [options.effects];

              Drupal.textimate(options);
            }
          });
        }
        else {
          if (once('textimate', animation.selector).length) {
            Drupal.textimate(options);
          }
        }

      });

    }
  };

})(jQuery, Drupal, drupalSettings, once);
