<?php

namespace Drupal\textimate;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\textimate\Form\TextimateFilter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Displays Animate CSS selector.
 *
 * @internal
 */
class TextimateAdmin extends FormBase {

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Animate manager.
   *
   * @var \Drupal\textimate\TextimateManagerInterface
   */
  protected $textimateManager;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a new Animate object.
   *
   * @param \Drupal\textimate\TextimateManagerInterface $textimate_manager
   *   The Animate selector manager.
   * @param \Symfony\Component\HttpFoundation\Request $current_request
   *   The current request.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(TextimateManagerInterface $textimate_manager, Request $current_request, FormBuilderInterface $form_builder, DateFormatterInterface $date_formatter) {
    $this->textimateManager = $textimate_manager;
    $this->currentRequest = $current_request;
    $this->formBuilder = $form_builder;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('textimate.effect_manager'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('form_builder'),
      $container->get('date.formatter'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'textimate_admin_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Attach Textimate overview admin library.
    $form['#attached']['library'][] = 'textimate/textimate.list';

    $search = $this->currentRequest->query->get('search');
    $status = $this->currentRequest->query->get('status') ?? NULL;

    /** @var \Drupal\textimate\Form\TextimateFilter $form */
    $form['textimate_admin_filter_form'] = $this->formBuilder->getForm(TextimateFilter::class, $search, $status);
    $form['#attributes']['class'][] = 'textimate-filter';
    $form['#attributes']['class'][] = 'views-exposed-form';

    $header = [
      [
        'data'  => $this->t('Selector'),
        'field' => 't.tid',
      ],
      [
        'data'  => $this->t('Label'),
        'field' => 't.label',
      ],
      [
        'data'  => $this->t('Status'),
        'field' => 't.status',
      ],
      [
        'data'  => $this->t('Updated'),
        'field' => 't.changed',
        'sort'  => 'desc',
      ],
      $this->t('Operations'),
    ];

    $rows = [];
    $result = $this->textimateManager->findAll($header, $search, $status);
    foreach ($result as $textimate) {
      $row = [];
      $row['selector'] = $textimate->selector;
      $row['label'] = $textimate->label;
      $status_class = $textimate->status ? 'marker marker--enabled' : 'marker';
      $row['status'] = [
        'data' => [
          '#type' => 'markup',
          '#prefix' => '<span class="' . $status_class . '">',
          '#suffix' => '</span>',
          '#markup' => $textimate->status ? $this->t('Enabled') : $this->t('Disabled'),
        ],
      ];
      $row['update'] = $this->dateFormatter->format($textimate->changed, 'short');
      $links = [];
      $links['edit'] = [
        'title' => $this->t('Edit'),
        'url'   => Url::fromRoute('textimate.edit', ['tid' => $textimate->tid]),
      ];
      $links['delete'] = [
        'title' => $this->t('Delete'),
        'url'   => Url::fromRoute('textimate.delete', ['tid' => $textimate->tid]),
      ];
      $links['duplicate'] = [
        'title' => $this->t('Duplicate'),
        'url'   => Url::fromRoute('textimate.duplicate', ['tid' => $textimate->tid]),
      ];
      $row[] = [
        'data' => [
          '#type'  => 'operations',
          '#links' => $links,
        ],
      ];
      $rows[] = $row;
    }

    $form['textimate_admin_table'] = [
      '#type'   => 'table',
      '#header' => $header,
      '#rows'   => $rows,
      '#empty'  => $this->t('No textimate effect available. <a href=":link">Add effect</a> with textimate.', [
        ':link' => Url::fromRoute('textimate.add')
          ->toString(),
      ]),
      '#attributes' => ['class' => ['textimate-list']],
    ];

    $form['pager'] = ['#type' => 'pager'];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @todo Add operations to textimate list
  }

}
