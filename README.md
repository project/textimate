INTRODUCTION
------------

Textimate is a innovative and powerful Drupal module combines the
words "text" and "animate" to deliver an experience that's nothing
short of amazing. it's the bridge between static content and captivating
user experiences.

By seamlessly merging "text" and "animate" Textimate empowers you to
effortlessly transform mundane text into mesmerizing animations.

Textimate empowers you to effortlessly transform ordinary text into
extraordinary animations. Elevate your website's visual appeal and leave
your audience awestruck. With just a few clicks, you can infuse life
into your content, creating an immersive user experience that resonates.

Whether you're a seasoned developer or a first-time Drupal user,
Textimate is intuitively crafted to ensure a seamless integration into
your project. Unlock a world of creative potential with a tool that's as
versatile as it is powerful.


FEATURES
--------

'Animate.css' library is:

  - Simply Ease-to-Use

  - Lightweight and Efficient

  - Diverse text animation effects

  - Cross-browser and Responsive

  - Usage with Javascript

  - Customizable


REQUIREMENTS
------------

'Splitting' module:

  - https://www.drupal.org/project/splitting


'AnimateCSS' module:

  - https://www.drupal.org/project/animatecss


INSTALLATION
------------

Note: If you don't already have the required modules of Textimate,
download and install them. To install the required modules,
you can read installation guid from the "Readme.md" file of those modules.

1. Download 'Textimate' module:
   - https://www.drupal.org/project/textimate

2. Extract and place it in the root of contributed modules directory i.e.
   /modules/contrib/textimate or /modules/textimate

3. Go to Extend menu "/admin/modules" in your Drupal administration.

4. Select and install 'Textimate' module to enable it.

5. Go to "Textimate" admin config page '/admin/config/user-interface/textimate'
   in your Drupal administration configuration menu to change settings.


USAGE
-----

1. Go to "Text animate" admin overview page '/admin/structure/textimate'
   in your Drupal administration structure menu.

2. Click on "Add effect"

3. Enter valid selector, you can use HTML tag, class with dot(.)
   and ID with hash(#) prefix. Make sure your selector has plain text content.
   e.g. ".page-title" or ".block-title" or ".main-content h2" etc.

3. Then you can add one or more animation by select animation name and
   click on "Add animation" for more effects.

4. You can change some setting option if you need such as:

   - Auto start:
     Animation will start automatically, or you can start manual in your
     theme javascript file.

   - Loop:
     If enabled, element will repeat the animation.

   - Initial Delay:
     Delay before starting your animation.

   - Min display time:
     The minimum display time for each effect before it is replaced.

   - Splitting type:
     The effect type by words, characters or lines animation.

   - Javascript event:
     The javascript event to show animation.

5. Save effects and that's it!

6. Enjoy text animation with Textimate!


How does it Work?
-----------------

1. Enable "Textimate" module, Follow INSTALLATION in above.

2. Pick text selector you want to animate to it by using right-click on your
   website, choose "Inspect" or "View page source (CTRL + U)" of context menu
   in your Chrome browser and find exact valid text class, ID or HTML tag.

   - Use class with dot(.) and ID with hash(#) prefix or HTML tags.
     e.g. ".page-title", ".block-title", "h2" or #myCustomID etc.

   - Nested selector is allowed.
     e.g. ".main-content h3", "body.page-node-type-article h1.page-title"

   * IMPORTANT: Your text should be a plain text like title above.

3. Add effects in admin to animate your website text, Follow USAGE in above.

4. Textimate using Splitting module to split text to characters or words etc.
   and with AnimateCSS module functionality and library will add animation
   to your text each character or words etc.

5. Refresh your website where that your selector is there.

6. Enjoy that.

Animations can improve the UX of an interface, but keep in mind that they can
also get in the way of your users!


MAINTAINERS
-----------

Current module maintainer:

 * Mahyar Sabeti - https://www.drupal.org/u/mahyarsbt
