/**
 * @file
 * Textimate form behaviors.
 */

(function ($, Drupal, drupalSettings, once) {

  "use strict";

  Drupal.behaviors.textimateForm = {
    attach: function (context, settings) {

      var $context = $(context);
      $context.find('#edit-options').drupalSetSummary(function (context) {
        var values = [];
        var minDisplayTime = Drupal.checkPlain($(context).find('#edit-min-display-time')[0].value) || 0;
        values.push(Drupal.t('Min display time: @minDisplayTime', {'@minDisplayTime': minDisplayTime}));
        var initialDelay = Drupal.checkPlain($(context).find('#edit-initial-delay')[0].value) || 0;
        values.push(Drupal.t('Initial delay: @initialDelay', {'@initialDelay': initialDelay}));
        if ($(context).find('#edit-loop').is(':checked')) {
          values.unshift(Drupal.t('Loop'));
        }
        if ($(context).find('#edit-auto-start').is(':checked')) {
          values.unshift(Drupal.t('Auto start'));
        } else {
          values.unshift(Drupal.t('Not start'));
        }
        return values.join(', ');
      });
      $context.find('#edit-advanced').drupalSetSummary(function (context) {
        var values = []
          , splitType = $(context).find('#edit-split option:selected').text()
          , eventType = $(context).find('#edit-event option:selected').text();
        values.push(Drupal.t('Javascript event: @eventType', {'@eventType': eventType}));
        values.push(Drupal.t('Splitting type: @splitType', {'@splitType': splitType}));
        return values.join(', ');
      });
      $context.find('#edit-administration').drupalSetSummary(function (context) {
        var values = [];
        if ($(context).find('#edit-status').is(':checked')) {
          values.unshift(Drupal.t('Enabled'));
        } else {
          values.unshift(Drupal.t('Disabled'));
        }
        return values.join(', ');
      });

      $(once('textimate-form-tr', '#textimate-form tr', context)).each(function (index) {

        let currentRow = $(this)
          , preview = currentRow.find('p.textimate-now')
          , select = currentRow.find('div.form-item select.textimate-effect')
          , cancel = currentRow.find('input.textimate-cancel')
          , update = currentRow.find('input.textimate-update')
          , setting = currentRow.find('input.textimate-edit')
          , options = currentRow.find('div.textimate-options');

        if (options.closest('td').length) {
          options.closest('td').hide();
        }

        select.bind('change', Drupal.textimatePreview);

        preview.bind({
          click: function (e) {
            // Click event handler.
            select.trigger('change');
          },
        });

        let delayScale = currentRow.find('.textimate-delay-scale').val()
          , delay = currentRow.find('.textimate-delay').val()
          , mode = currentRow.find('.textimate-mode').val();

        setting.bind({
          click: function (e) {
            Drupal.textimateShowColumns($(this));
            e.preventDefault();
          },
        });

        cancel.bind({
          click: function (e) {
            Drupal.textimateHideColumns($(this));
            currentRow.find('.textimate-delay-scale').val(delayScale);
            currentRow.find('.textimate-delay').val(delay);
            currentRow.find('.textimate-mode').val(mode);
            e.preventDefault();
          },
        });

        update.bind({
          click: function (e) {
            Drupal.textimateHideColumns($(this));
            delayScale = currentRow.find('.textimate-delay-scale').val();
            delay = currentRow.find('.textimate-delay').val();
            mode = currentRow.find('.textimate-mode').val();

            let modeTxt = currentRow.find('.textimate-mode option:selected').text();
            let summary = Drupal.t('Delay Scale: @delayScale', {'@delayScale': delayScale}) + "<br>"
                        + Drupal.t('Delay: @delay', {'@delay': delay}) + "<br>"
                        + Drupal.t('Mode: @mode', {'@mode': modeTxt});

            currentRow.find('.textimate-summary').html(summary);

            // Preview event handler.
            select.trigger('change');
            e.preventDefault();
          },
        });

      });

      $('#edit-effects-more').bind({
        mousedown: function (e) {
          let selectorLabel = $('.form-item--selector label');
          let selectorField = $('.form-item--selector input');
          if (selectorField.val().trim() === '') {
            selectorLabel.addClass('has-error');
            selectorField.addClass('error');
          }
          else {
            if (selectorLabel.hasClass('has-error')) {
              selectorLabel.removeClass('has-error');
              selectorField.removeClass('error');
            }
          }
          e.preventDefault();
        },
      });

    }
  };

  /**
   * Shows/hides options forms for various textimate.
   *
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the show/hide functionality to each table row in the textimate edit form.
   */
  Drupal.textimatePreview = function () {
    let animation = $(this).closest('tr');
    let options = {
      selector: animation.find('.textimate-now'),
      loop: false,
      autoStart: true,
      minDisplayTime: $('.textimate-display-time').val(),
      initialDelay: $('.textimate-initial-delay').val(),
      // custom set of effects. This effects whether the
      // character is shown/hidden before or after an animation.
      effects: [{
        name: $(this).val(),
        delayScale: animation.find('.textimate-delay-scale').val(),
        delay: animation.find('.textimate-delay').val(),
        sync: animation.find('.textimate-mode').val() === 'sync',
        shuffle: animation.find('.textimate-mode').val() === 'shuffle',
        reverse: animation.find('.textimate-mode').val() === 'reverse',
      }],
      type: $('.textimate-split').val(),
      event: $('.textimate-event').val(),
    };

    setTimeout(function () {
      Drupal.textimate(options);
    }, 10);

  };

  /**
   * Hide the columns containing weight/parent form elements.
   *
   * Undo showColumns().
   */
  Drupal.textimateHideColumns = function (cancel) {
    const $tables = cancel.closest('tr');
    $tables.removeClass('field-plugin-settings-editing');
    // Show weight/parent cells and headers.
    $tables.find('.textimate-edit').closest('td').show();
    $tables.find('.textimate-delete').closest('td').show();
    $tables.find('.textimate-options').closest('td').hide();
    $('.th__').show();
  };

  /**
   * Show the columns containing weight/parent form elements.
   *
   * Undo hideColumns().
   */
  Drupal.textimateShowColumns = function (setting) {
    const $tables = setting.closest('tr');
    $tables.addClass('field-plugin-settings-editing');
    // Show weight/parent cells and headers.
    setting.closest('td').hide();
    $tables.find('.textimate-delete').closest('td').hide();
    $tables.find('.textimate-options').closest('td').show();
    $('.th__').hide();
  };

})(jQuery, Drupal, drupalSettings, once);
