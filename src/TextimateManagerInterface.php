<?php

namespace Drupal\textimate;

/**
 * Provides an interface defining a textimate manager.
 */
interface TextimateManagerInterface {

  /**
   * Returns if this textimate css selector is added.
   *
   * @param string $selector
   *   The textimate css selector to check.
   *
   * @return bool
   *   TRUE if the textimate css selector is added, FALSE otherwise.
   */
  public function isTextimate($selector);

  /**
   * Finds an added textimate css selector by its ID.
   *
   * @return string|false
   *   Either the added textimate selector or FALSE if none exist with that ID.
   */
  public function loadTextimate();

  /**
   * Add a textimate css selector.
   *
   * @param int $textimate_id
   *   The textimate id for edit.
   * @param string $selector
   *   The textimate selector to add.
   * @param string $label
   *   The label of textimate selector.
   * @param string $comment
   *   The comment for textimate options.
   * @param int $changed
   *   The expected modification time.
   * @param int $status
   *   The status for textimate.
   * @param string $options
   *   The textimate selector options.
   *
   * @return int|null|string
   *   The last insert ID of the query, if one exists.
   */
  public function addTextimate($textimate_id, $selector, $label, $comment, $changed, $status, $options);

  /**
   * Remove a textimate css selector.
   *
   * @param int $textimate_id
   *   The textimate id to remove.
   */
  public function removeTextimate($textimate_id);

  /**
   * Finds all added textimate css selector.
   *
   * @param array $header
   *   The textimate header to sort selector and label.
   * @param string $search
   *   The textimate search key to filter selector.
   * @param int|null $status
   *   The textimate status to filter selector.
   *
   * @return \Drupal\Core\Database\StatementInterface
   *   The result of the database query.
   */
  public function findAll(array $header, $search, $status);

  /**
   * Finds an added textimate css selector by its ID.
   *
   * @param int $textimate_id
   *   The ID for an added textimate selector.
   *
   * @return string|false
   *   Either the added textimate selector or FALSE if none exist with that ID.
   */
  public function findById($textimate_id);

}
