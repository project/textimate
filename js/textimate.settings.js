/**
 * @file
 * Contains definition of the behaviour Animate.css.
 */

(function ($, Drupal, drupalSettings, once) {
  "use strict";

  Drupal.behaviors.textimateAdmin = {
    attach: function (context, settings) {

      if (once('textimate__sample', '.textimate__sample').length) {
        let effectSample = $('p.textimate__sample')
          , effectReplay = $('input.textimate__replay');

        // Textimate preview replay.
        effectReplay.bind('click', function (event) {
          let options = {
            selector: effectSample,
            autoStart: true,
            loop: $('.textimate-loop').is(':checked'),
            minDisplayTime: $('.textimate-display-time').val(),
            initialDelay: $('.textimate-initial-delay').val(),
            // custom set of effects. This effects whether the
            // character is shown/hidden before or after an animation.
            effects: [{
              name: $('.textimate-effect').val(),
              delayScale: $('.textimate-delay-scale').val(),
              delay: $('.textimate-delay').val(),
              sync: $('.textimate-mode').val() === 'sync',
              shuffle: $('.textimate-mode').val() === 'shuffle',
              reverse: $('.textimate-mode').val() === 'reverse',
            }],
            type: $('.textimate-split').val(),
            event: $('.textimate-event').val(),
          };

          setTimeout(function () {
            Drupal.textimate(options);
          }, 10);

          event.preventDefault();
        }).trigger('click');
      }

      if (once('textimate__about', '.textimate__about').length) {
        new Drupal.textimateHelp();
      }

    }
  };

  Drupal.textimateHelp = function () {

    Drupal.textimate({
      selector: '.page-title',
      autoStart: true,
      loop: true,
      minDisplayTime: '2000',
      initialDelay: 0,
      effects: [
        {
          name: 'bounceIn',
          delayScale: '1.5',
          delay: '50',
          sync: false,
          shuffle: false,
          reverse: false,
        },
        {
          name: 'flip',
          delayScale: '1.5',
          delay: '50',
          sync: false,
          shuffle: false,
          reverse: false,
        },
        {
          name: 'flipOutY',
          delayScale: '1.5',
          delay: '50',
          sync: false,
          shuffle: false,
          reverse: true,
        },
        {
          name: 'zoomIn',
          delayScale: '1.5',
          delay: '50',
          sync: false,
          shuffle: false,
          reverse: false,
        },
        {
          name: 'heartBeat',
          delayScale: '1.5',
          delay: '50',
          sync: false,
          shuffle: false,
          reverse: true,
        },
        {
          name: 'fadeOut',
          delayScale: '1.5',
          delay: '50',
          sync: false,
          shuffle: true,
          reverse: false,
        },
      ],
      type: 'char',
    });

    Drupal.textimate({
      selector: '.textimate__about',
      initialDelay: 300,
      effects: [
        {
          name: 'jackInTheBox',
        },
        {
          name: 'swing',
        },
      ],
    });

    Drupal.textimate({
      selector: '.textimate__text',
      initialDelay: 600,
      effects: [
        {
          name: 'zoomInRight',
          delayScale: '1.2',
          delay: '99',
        },
      ],
      type: 'word',
    });

    Drupal.textimate({
      selector: '.textimate__usage',
      initialDelay: 1200,
      effects: [
        {
          name: 'jackInTheBox',
        },
        {
          name: 'swing',
        },
      ],
    });

    Drupal.textimate({
      selector: '.textimate__title',
      initialDelay: 1500,
      effects: [
        {
          name: 'lightSpeedInLeft',
        },
      ],
    });

    Drupal.textimate({
      selector: '.textimate__line',
      initialDelay: 2700,
      effects: [
        {
          name: 'fadeInRightBig',
          delayScale: '6',
          delay: '99',
        },
      ],
      type: 'line',
    });

    Drupal.textimate({
      selector: '.textimate__list',
      initialDelay: 3300,
      effects: [
        {
          name: 'slideInUp',
          delayScale: '6',
          delay: '66',
        },
      ],
      type: 'item',
    });

    Drupal.textimate({
      selector: '.textimate__warning',
      autoStart: true,
      loop: true,
      minDisplayTime: 0,
      initialDelay: 6000,
      effects: [
        {
          name: 'flipInX',
          delayScale: '1.5',
          delay: '30',
        },
        {
          name: 'tada',
          delayScale: '1.5',
          delay: '30',
          sync: false,
          shuffle: false,
          reverse: true,
        },
        {
          name: 'flash',
          delayScale: '1.5',
          delay: '30',
          sync: false,
          shuffle: true,
          reverse: false,
        },
        {
          name: 'wobble',
          delayScale: '1.5',
          delay: '30',
          sync: false,
          shuffle: false,
          reverse: true,
        },
        {
          name: 'bounce',
          delayScale: '1.5',
          delay: '30',
          sync: false,
          shuffle: true,
          reverse: false,
        },
        {
          name: 'jello',
          delayScale: '1.5',
          delay: '30',
          sync: false,
          shuffle: false,
          reverse: true,
        },
        {
          name: 'zoomOut',
          delayScale: '1',
          delay: '30',
        },
      ],
      type: 'char',
    });
  }

})(jQuery, Drupal, drupalSettings, once);
