/**
 * @file
 * Contains definition of the behaviour Animate.css.
 */

(function ($, Drupal, drupalSettings, once) {
  "use strict";

  Drupal.behaviors.textimate = {
    attach: function (context, settings) {

      const defaults = {
        selector: '.texts',
        type: 'char',
        loop: false,
        minDisplayTime: 2000,
        initialDelay: 0,
        in: {
          effect: 'fadeInLeftBig',
          delayScale: 1.5,
          delay: 50,
          sync: false,
          reverse: false,
          shuffle: false,
          callback: function () {}
        },
        out: {
          effect: 'hinge',
          delayScale: 1.5,
          delay: 50,
          sync: false,
          reverse: false,
          shuffle: false,
          callback: function () {}
        },
        autoStart: true,
        inEffects: [],
        outEffects: [ 'hinge' ],
        callback: function () {},
      };
    }
  };

  Drupal.textimate = function (options) {
    let $selector = $(options.selector);
    $selector.textimate(options);
  };

  var Textimate = function (element, options) {
    var base = this
      , $element = $(element);

    base.init = function () {

      // Restore selector text if already triggered.
      if ($element.find('.texts').length) {
        let originalText = $element.find('.texts');
        $element.html(originalText.find(':first-child').html());
      }

      base.$texts = $element.find(options.selector);

      if ($element.hasClass( "splitting" )) {
        if (!base.$texts.length) {
          let dataWords = '';
          $element.find('.word').each(function () {
            dataWords = dataWords + ' ' + $(this).data('word');
          });
          base.$texts = $('<ul class="texts"><li>' + dataWords.trim() + '</li></ul>');
          $element.html(base.$texts);
          $element.removeClass( "words chars lines splitting" );
        }
      } else {
        if (!base.$texts.length) {
          base.$texts = $('<ul class="texts"><li>' + $element.html() + '</li></ul>');
          $element.html(base.$texts);
        }
      }

      base.$texts.hide();

      base.$current = $('<span>')
        .html(base.$texts.find(':first-child').html())
        .prependTo($element);

      switch (options.type) {
        case 'item':
          var items = Splitting({
            target: base.$current,
            by: 'items',
            matching: 'li'
          });
          items.forEach((splitResult) => {
            splitResult.items.forEach((element) => {
              element.classList.add('item');
            });
            console.log(splitResult.items);
          });
          break;

        case 'line':
          var lines = Splitting({
            target: base.$current,
            by: options.type + 's'
          });
          lines.forEach((splitResult) => {
            const wrappedLines = splitResult.lines.map(
              (wordsArr) => `
              <span class="line">
                  <div class="words">
                      ${wordsArr.map((word) => `${word.outerHTML}<span class="whitespace"> </span>`).join("")}
                  </div>
              </span>
            `
            ).join("");
            splitResult.el.innerHTML = wrappedLines;
          });
          break;

        default:
          Splitting({
            target: base.$current,
            by: options.type + 's'
          });
          break;
      }

      splittingStyle($element, base.$current);

      if (isInEffect(options.effects[0].name)) {
        base.$current.css('visibility', 'hidden');
      } else {
        base.$current.css('visibility', 'visible');
      }

      base.setOptions(options);

      base.timeoutRun = null;

      switch(base.options.event) {
        case 'load':
        case 'onload':
          setTimeout(function () {
            base.options.autoStart && base.start();
          }, base.options.initialDelay)
          break;

        case 'scroll':
          let scrolling = true;
          $(window).scroll(function () {
            if (base.$current.length && base.$current.isInViewport()) {
              setTimeout(function () {
                if (scrolling) {
                  scrolling = false;
                  base.start();
                }
              }, base.options.initialDelay)
            }
          });
          break;

        case 'mouseover':
          base.$current
            .on( "mouseenter", function () {
              base.timeoutRun = setTimeout(function () {
                base.start();
              }, base.options.initialDelay)
            })
            .on( "mouseleave", function () {
              base.currentEvent = base.triggerEvent('end').type;
              base.stop();
            });
          break;

        default:
          base.$current.on(base.options.event, function () {
            setTimeout(function () {
              base.start();
            }, base.options.initialDelay)
          });
      }
    };

    base.setOptions = function (options) {
      base.options = options;
    };

    base.triggerEvent = function (name) {
      var e = $.Event(name + '.tlt');
      $element.trigger(e, base);
      return e;
    };

    base.do = function (index, cb) {
      index = index || 0;

      var $elem = base.$texts.find(':nth-child(' + ((index||0) + 1) + ')')
        , options = $.extend(true, {}, base.options, $elem.length ? getData($elem[0]) : {})
        , $targets;

      if (base.currentEvent == 'end') {
        return;
      }

      $elem.addClass('current');

      base.triggerEvent('animationBegin');
      $element.attr('data-active', $elem.data('id'));

      $targets = $(options.selector)
        .find('[class="' + base.options.type + '"]');

      switch (base.options.type) {
        case 'item':
        case 'line':
          $targets.css('display', 'block');
          break;

        default:
          $targets.css('display', 'inline-block');
          break;
      }

      if (isInEffect(options.effects[index].name)) {
        $targets.css('visibility', 'hidden');
      } else {
        $targets.css('visibility', 'visible');
      }

      base.currentIndex = index;

      animateTargets($targets, options.effects[index], options.compat, function () {
        $elem.removeClass('current');
        base.triggerEvent('animationEnd');
        if (options.effects[index].callback) {
          options.effects[index].callback();
        }
        if (cb) {
          cb(base);
        }
      });
    };

    base.start = function (index) {
      setTimeout(function () {
        base.currentEvent = base.triggerEvent('start').type;

        let length = base.options.effects.length;
        (function run(index) {
          base.do(index, function () {
            index += 1;

            if (!base.options.loop && index >= length) {
              base.currentEvent = base.triggerEvent('end').type;
            }
            else {
              index = index % length;

              base.timeoutRun = setTimeout(function () {
                run(index);
              }, base.options.minDisplayTime);
            }

          });
        }(index || 0));

      }, base.options.initialDelay);
    };

    base.stop = function () {
      if (base.timeoutRun) {
        clearInterval(base.timeoutRun);
        base.timeoutRun = null;
      }
    };

    base.init();
  }

  function getData(node) {
    let attrs = node.attributes || []
      , data = {};

    if (!attrs.length) {
      return data;
    }

    $.each(attrs, function (i, attr) {
      var nodeName = attr.nodeName.replace(/delayscale/, 'delayScale');
      if (/^data-in-*/.test(nodeName)) {
        data.in = data.in || {};
        data.in[nodeName.replace(/data-in-/, '')] = stringToBoolean(attr.nodeValue);
      } else if (/^data-out-*/.test(nodeName)) {
        data.out = data.out || {};
        data.out[nodeName.replace(/data-out-/, '')] = stringToBoolean(attr.nodeValue);
      } else if (/^data-*/.test(nodeName)) {
        data[nodeName.replace(/data-/, '')] = stringToBoolean(attr.nodeValue);
      }
    })

    return data;
  }

  function splittingStyle($element, $current) {
    if ($element.hasClass( "animated" )) {
      $element.removeClass( "animated" );
      $current.addClass( "rainbow" )
    }
    if ($element.hasClass( "color" )) {
      $element.removeClass( "color" );
      $current.addClass( "color" )
    }
    if ($element.hasClass( "rainbow" )) {
      $element.removeClass( "rainbow" );
      $current.addClass( "rainbow" )
    }
    if ($element.hasClass( "paper" )) {
      $element.removeClass( "paper" );
      $current.addClass( "paper" )
    }
    if ($element.hasClass( "blooming" )) {
      $element.removeClass( "blooming" );
      $current.addClass( "blooming" )
    }
    if ($element.hasClass( "overlap" )) {
      $element.removeClass( "overlap" );
    }
    if ($element.hasClass( "peeled" )) {
      $element.removeClass( "peeled" );
      $current.addClass( "peeled" )
    }
  }

  function stringToBoolean(str) {
    if (str !== "true" && str !== "false") {
      return str;
    }
    return (str === "true");
  }

  function isInEffect(effect) {
    return (effect.indexOf("In") >= 0) || /In/.test(effect) || $.inArray(effect, $.fn.textimate.defaults.inEffects) >= 0;
  }

  function isOutEffect(effect) {
    return (effect.indexOf("Out") >= 0) || /Out/.test(effect) || $.inArray(effect, $.fn.textimate.defaults.outEffects) >= 0;
  }

  function shuffle(o) {
    for (var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
  }

  function animate($target, name, compat, cb) {
    var Prefix = compat ? '' : 'animate__';

    $target.addClass(Prefix + 'animated ' + Prefix + name)
      .css('visibility', 'visible')
      .show();

    $target.one('animationend webkitAnimationEnd oAnimationEnd', function () {
      $target.removeClass(Prefix + 'animated ' + Prefix + name);
      cb && cb();
    });
  }

  function animateTargets($targets, options, compat, cb) {
    var that = this
      , count = $targets.length;

    if (!count) {
      cb && cb();
      return;
    }

    if (options.shuffle) {
      $targets = shuffle($targets);
    }
    if (options.reverse) {
      $targets = $targets.toArray().reverse();
    }

    $.each($targets, function (i, t) {
      let $target = $(t);

      function complete() {
        if (isInEffect(options.name)) {
          $target.css('visibility', 'visible');
        } else if (isOutEffect(options.name)) {
          $target.css('visibility', 'hidden');
        }
        count -= 1;
        if (!count && cb) {
          cb();
        }
      }

      let delay = options.sync ? options.delay : options.delay * i * options.delayScale;

      $target.text() ?
        setTimeout(function () { animate($target, options.name, compat, complete) }, delay) :
        complete();
    });
  }

  $.fn.textimate = function (settings, args) {
    return this.each(function () {
      let $this = $(this)
        , data = $this.data('textimate')
        , options = $.extend(true, {}, $.fn.textimate.defaults, getData(this), typeof settings == 'object' && settings);

      $this.data('textimate', (data = new Textimate(this, options)));
    })
  };

  $.fn.textimate.defaults = {
    selector: '.texts',
    autoStart: true,
    loop: false,
    minDisplayTime: 2000,
    initialDelay: 0,
    effects: [
      {
        name: 'fadeInLeftBig',
        delayScale: 1.5,
        delay: 50,
        sync: false,
        reverse: false,
        shuffle: false,
        callback: function () {}
      },
    ],
    callback: function () {},
    type: 'char',
    event: 'onload',
    compat: false,
  };

  $.fn.isInViewport = function () {
    let elementTop = $(this).offset().top;
    let elementBottom = elementTop + $(this).outerHeight();
    let viewportTop = $(window).scrollTop();
    let viewportBottom = viewportTop + $(window).height();
    return elementBottom > viewportTop && elementTop < viewportBottom;
  };

})(jQuery, Drupal, drupalSettings, once);
