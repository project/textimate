<?php

namespace Drupal\textimate\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\textimate\TextimateManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a form to remove CSS selector.
 *
 * @internal
 */
class TextimateDelete extends ConfirmFormBase {

  /**
   * The Animate selector.
   *
   * @var int
   */
  protected $textimate;

  /**
   * The Animate selector manager.
   *
   * @var \Drupal\textimate\TextimateManagerInterface
   */
  protected $textimateManager;

  /**
   * Constructs a new textimateDelete object.
   *
   * @param \Drupal\textimate\TextimateManagerInterface $textimate_manager
   *   The Animate selector manager.
   */
  public function __construct(TextimateManagerInterface $textimate_manager) {
    $this->textimateManager = $textimate_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('textimate.effect_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'textimate_selector_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to remove %selector from textimate selectors?', ['%selector' => $this->textimate['selector']]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param int $tid
   *   The Textimate record ID to remove.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $tid = 0) {
    if (!$this->textimate = $this->textimateManager->findById($tid)) {
      throw new NotFoundHttpException();
    }
    $form['textimate_id'] = [
      '#type'  => 'value',
      '#value' => $tid,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $textimate_id = $form_state->getValue('textimate_id');
    $this->textimateManager->removeTextimate($textimate_id);
    $this->logger('user')
      ->notice('Deleted %selector', ['%selector' => $this->textimate['selector']]);
    $this->messenger()
      ->addStatus($this->t('The textimate selector %selector was deleted.', ['%selector' => $this->textimate['selector']]));

    // Flush caches so the updated config can be checked.
    drupal_flush_all_caches();

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('textimate.admin');
  }

}
