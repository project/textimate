<?php

namespace Drupal\textimate;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\PagerSelectExtender;
use Drupal\Core\Database\Query\TableSortExtender;

/**
 * Textimate manager.
 */
class TextimateManager implements TextimateManagerInterface {

  /**
   * The database connection used to check the selector against.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a TextimateManager object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection which will be used to check the selector
   *   against.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public function isTextimate($selector) {
    return (bool) $this->connection->query("SELECT * FROM {textimate} WHERE [selector] = :selector", [':selector' => $selector])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function loadTextimate() {
    $query = $this->connection
      ->select('textimate', 't')
      ->fields('t', ['tid', 'selector', 'options'])
      ->condition('status', 1);

    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function addTextimate($textimate_id, $selector, $label, $comment, $changed, $status, $options) {
    $this->connection->merge('textimate')
      ->key('tid', $textimate_id)
      ->fields([
        'selector' => $selector,
        'label'    => $label,
        'comment'  => $comment,
        'changed'  => $changed,
        'status'   => $status,
        'options'  => $options,
      ])
      ->execute();

    return $this->connection->lastInsertId();
  }

  /**
   * {@inheritdoc}
   */
  public function removeTextimate($textimate_id) {
    $this->connection->delete('textimate')
      ->condition('tid', $textimate_id)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function findAll($header = [], $search = '', $status = NULL) {
    $query = $this->connection
      ->select('textimate', 't')
      ->extend(PagerSelectExtender::class)
      ->extend(TableSortExtender::class)
      ->orderByHeader($header)
      ->limit(50)
      ->fields('t');

    if (!empty($search) && !empty(trim((string) $search)) && $search !== NULL) {
      $search = trim((string) $search);
      // Escape for LIKE matching.
      $search = $this->connection->escapeLike($search);
      // Replace wildcards with MySQL/PostgreSQL wildcards.
      $search = preg_replace('!\*+!', '%', $search);
      // Add selector and the label field columns.
      $group = $query->orConditionGroup()
        ->condition('selector', '%' . $search . '%', 'LIKE')
        ->condition('label', '%' . $search . '%', 'LIKE');
      // Run the query to find matching targets.
      $query->condition($group);
    }

    // Check if status is set.
    if (!is_null($status) && $status != '') {
      $query->condition('status', $status);
    }

    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function findById($textimate_id) {
    return $this->connection->query("SELECT [selector], [label], [comment], [status], [options] FROM {textimate} WHERE [tid] = :tid", [':tid' => $textimate_id])
      ->fetchAssoc();
  }

}
