<?php

namespace Drupal\textimate\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures text animate settings.
 */
class TextimateSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'textimate_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Textimate flags for specific form labels and suffix.
    $experimental_label = ' <span class="textimate-experimental-flag">Experimental</span>';
    $ms_unit_label = ' <span class="textimate-unit-flag">ms</span>';

    // Get current settings.
    $config = $this->config('textimate.settings');

    // Textimate module configuration.
    $form['settings'] = [
      '#type'  => 'details',
      '#title' => $this->t('Textimate config'),
      '#open'  => TRUE,
    ];

    // Let module handle load Textimate plugin.
    $form['settings']['load'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Load Textimate'),
      '#default_value' => $config->get('load'),
      '#description'   => $this->t("If enabled, this module will attempt to load the textimate plugin for your site."),
    ];

    // Load Textimate plugin Per-path.
    $form['settings']['url'] = [
      '#type'        => 'fieldset',
      '#title'       => $this->t('Load on specific URLs'),
      '#collapsible' => TRUE,
      '#collapsed'   => TRUE,
    ];
    $form['settings']['url']['url_visibility'] = [
      '#type'          => 'radios',
      '#title'         => $this->t('Load textimate on specific pages'),
      '#options'       => [
        0 => $this->t('All pages except those listed'),
        1 => $this->t('Only the listed pages'),
      ],
      '#default_value' => $config->get('url.visibility'),
    ];
    $form['settings']['url']['url_pages'] = [
      '#type'          => 'textarea',
      '#title'         => '<span class="element-invisible">' . $this->t('Pages') . '</span>',
      '#default_value' => _textimate_array_to_string($config->get('url.pages')),
      '#description'   => $this->t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. An example path is %admin-wildcard for every user page. %front is the front page.", [
        '%admin-wildcard' => '/admin/*',
        '%front'          => '<front>',
      ]),
    ];

    // Textimate specific options.
    $form['options'] = [
      '#type'  => 'details',
      '#title' => $this->t('Global settings'),
      '#open'  => TRUE,
    ];

    // List of selectors to individual Animate Control with textimate.
    $form['options']['selectors'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Selectors'),
      '#default_value' => _textimate_array_to_string($config->get('options.selector')),
      '#description'   => $this->t('Enter CSS selector (id/class) of your elements e.g., "#id" or ".classname". Use a new line for each selector.'),
      '#rows'          => 3,
    ];

    // The splitting type for textimate effects.
    $form['options']['split'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Type'),
      '#options'       => textimate_type_options(),
      '#default_value' => $config->get('options.split'),
      '#description'   => $this->t('Select the effect type by words, characters or lines animation.'),
      '#attributes'    => ['class' => ['textimate-split']],
    ];

    // The javascript event for textimate effects.
    $form['options']['event'] = [
      '#title'         => $this->t('Event') . $experimental_label,
      '#type'          => 'select',
      '#options'       => animatecss_event_options(),
      '#default_value' => $config->get('options.event'),
      '#description'   => $this->t('Select the javascript event to show animation.'),
      '#attributes'    => ['class' => ['textimate-event']],
    ];

    // Textimate animation default options.
    $form['options']['textimate'] = [
      '#title'       => $this->t('Default options'),
      '#type'        => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed'   => FALSE,
    ];

    // Set whether to automatically start animating.
    $form['options']['textimate']['auto_start'] = [
      '#title'         => $this->t('Auto start'),
      '#type'          => 'checkbox',
      '#default_value' => $config->get('options.autoStart'),
      '#description'   => $this->t('Set whether to automatically start animating.'),
      '#attributes'    => ['class' => ['textimate-auto-start']],
    ];

    // Enable animation looping.
    $form['options']['textimate']['loop'] = [
      '#title'         => $this->t('Loop'),
      '#type'          => 'checkbox',
      '#default_value' => $config->get('options.loop'),
      '#description'   => $this->t("If enabled, the animation will repeat for the selector."),
      '#attributes'    => ['class' => ['textimate-loop']],
    ];

    // Sets the initial delay before starting the animation.
    // Note that depending on the effect you may need to manually apply
    // visibility: hidden to the element before running this plugin.
    $form['options']['textimate']['initial_delay'] = [
      '#title'         => $this->t('Initial delay'),
      '#type'          => 'textfield',
      '#size'          => 9,
      '#maxlength'     => 9,
      '#default_value' => $config->get('options.initialDelay'),
      '#description'   => $this->t('Sets the initial delay before starting the animation.'),
      '#attributes'    => ['class' => ['textimate-initial-delay']],
      '#field_suffix'  => $ms_unit_label,
    ];

    // Sets the minimum display time for each effect before it is replaced.
    $form['options']['textimate']['min_display_time'] = [
      '#title'         => $this->t('Min display time'),
      '#type'          => 'textfield',
      '#size'          => 9,
      '#maxlength'     => 9,
      '#default_value' => $config->get('options.minDisplayTime'),
      '#description'   => $this->t('Sets the minimum display time for each effect before it is replaced.'),
      '#attributes'    => ['class' => ['textimate-display-time']],
      '#field_suffix'  => $ms_unit_label,
    ];

    // Default effect options.
    $form['options']['animation'] = [
      '#title'       => $this->t('Effect default options'),
      '#type'        => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed'   => FALSE,
    ];

    // Effect animation name.
    $form['options']['animation']['name'] = [
      '#title'         => $this->t('Animation name'),
      '#type'          => 'select',
      '#options'       => animatecss_animation_options(),
      '#default_value' => $config->get('options.effects.name'),
      '#description'   => $this->t('Select the animation name you want to use as a text animate.'),
      '#attributes'    => ['class' => ['textimate-effect']],
    ];

    // The delay factor applied to each consecutive character.
    $form['options']['animation']['delay_scale'] = [
      '#title'         => $this->t('Delay scale'),
      '#type'          => 'textfield',
      '#size'          => 6,
      '#maxlength'     => 6,
      '#default_value' => $config->get('options.effects.delayScale'),
      '#description'   => $this->t('Set the delay factor applied to each consecutive character.'),
      '#attributes'    => ['class' => ['textimate-delay-scale']],
    ];

    // Set the delay between each character.
    $form['options']['animation']['delay'] = [
      '#title'         => $this->t('Delay'),
      '#type'          => 'textfield',
      '#size'          => 6,
      '#maxlength'     => 6,
      '#default_value' => $config->get('options.effects.delay'),
      '#description'   => $this->t('Set the delay between each character.'),
      '#attributes'    => ['class' => ['textimate-delay']],
    ];

    // The animation character modes by:
    // sequence, randomize and reverse sync.
    $form['options']['animation']['mode'] = [
      '#title'         => $this->t('Motion mode'),
      '#type'          => 'select',
      '#options'       => textimate_mode_options(),
      '#default_value' => $config->get('options.mode'),
      '#description'   => $this->t('Select the display mode of the effect letters.'),
      '#attributes'    => ['class' => ['textimate-mode']],
    ];

    // Textimate preview.
    $form['preview'] = [
      '#type'   => 'details',
      '#title'  => $this->t('Effect preview'),
      '#open'   => TRUE,
    ];

    // Textimate effect sample text.
    $form['preview']['sample'] = [
      '#type'   => 'markup',
      '#markup' => '<div class="textimate__preview"><p class="textimate__sample">Textimate is a text animate!</p></div>',
    ];

    // Replay button for preview textimate current configs.
    $form['preview']['replay'] = [
      '#value'      => $this->t('Replay'),
      '#type'       => 'button',
      '#attributes' => ['class' => ['textimate__replay']],
    ];

    // The textimate settings library.
    $form['#attached']['library'][] = 'textimate/textimate.settings';

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    // Save the updated textimate settings.
    $this->config('textimate.settings')
      ->set('load', $values['load'])
      ->set('url.visibility', $values['url_visibility'])
      ->set('url.pages', _textimate_string_to_array($values['url_pages']))
      ->set('options.selector', _textimate_string_to_array($values['selectors']))
      ->set('options.split', $values['split'])
      ->set('options.event', $values['event'])
      ->set('options.mode', $values['mode'])
      ->set('options.loop', $values['loop'])
      ->set('options.minDisplayTime', $values['min_display_time'])
      ->set('options.initialDelay', $values['initial_delay'])
      ->set('options.autoStart', $values['auto_start'])
      ->set('options.effects.name', $values['name'])
      ->set('options.effects.delayScale', $values['delay_scale'])
      ->set('options.effects.delay', $values['delay'])
      ->set('options.effects.sync', $values['mode'] == 'sync')
      ->set('options.effects.shuffle', $values['mode'] == 'shuffle')
      ->set('options.effects.reverse', $values['mode'] == 'reverse')
      ->save();

    // Flush caches so the updated config can be checked.
    drupal_flush_all_caches();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'textimate.settings',
    ];
  }

}
