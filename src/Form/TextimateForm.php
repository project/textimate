<?php

namespace Drupal\textimate\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\textimate\TextimateManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Textimate add and edit effect form.
 *
 * @internal
 */
class TextimateForm extends FormBase {

  /**
   * Animate manager.
   *
   * @var \Drupal\textimate\TextimateManagerInterface
   */
  protected $textimateManager;

  /**
   * A config object for the system performance animation.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new textimate object.
   *
   * @param \Drupal\textimate\TextimateManagerInterface $textimate_manager
   *   The textimate selector manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A config factory for retrieving required config objects.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(TextimateManagerInterface $textimate_manager, ConfigFactoryInterface $config_factory, TimeInterface $time) {
    $this->textimateManager = $textimate_manager;
    $this->config = $config_factory->get('textimate.settings');
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('textimate.effect_manager'),
      $container->get('config.factory'),
      $container->get('datetime.time'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'textimate_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param int $tid
   *   (optional) Animate id to be passed on to
   *   \Drupal::formBuilder()->getForm() for use as the default value of the
   *   Animate ID form data.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $tid = 0) {
    // Textimate flags for specific form labels and suffix.
    $experimental_label = ' <span class="textimate-experimental-flag">Experimental</span>';
    $ms_unit_label = ' <span class="textimate-unit-flag">ms</span>';

    // Load the textimate configuration settings.
    $config = $this->config;

    // Prepare textimate form default values.
    $textimate = $this->textimateManager->findById($tid) ?? [];
    $selector  = $textimate['selector'] ?? '';
    $label     = $textimate['label'] ?? '';
    $comment   = $textimate['comment'] ?? '';
    $status    = $textimate['status'] ?? TRUE;
    $options   = [];

    // Handle the case when $textimate is not an array or option is not set.
    if (is_array($textimate) && isset($textimate['options'])) {
      $options = unserialize($textimate['options'], ['allowed_classes' => FALSE]) ?? '';
    }

    // Store textimate id.
    $form['textimate_id'] = [
      '#type'  => 'value',
      '#value' => $tid,
    ];

    // The default selector to use when detecting multiple texts to animate.
    $form['selector'] = [
      '#title'         => $this->t('Selector'),
      '#type'          => 'textfield',
      '#required'      => TRUE,
      '#size'          => 64,
      '#maxlength'     => 255,
      '#default_value' => $selector,
      '#description'   => $this->t('Here, you can use HTML tag, class with dot(.) and ID with hash(#) prefix. Make sure your selector has plain text content. e.g. ".page-title" or ".block-title".'),
      '#placeholder'   => $this->t('Enter valid selector'),
    ];

    // The label of this selector.
    $form['label'] = [
      '#title'         => $this->t('Label'),
      '#type'          => 'textfield',
      '#required'      => FALSE,
      '#size'          => 64,
      '#maxlength'     => 255,
      '#default_value' => $label ?? '',
      '#description'   => $this->t('Enter a short label for this selector. The label will be displayed on the Textimate effects administration overview page.'),
    ];

    // Create a table with each <tr> corresponding to an effect.
    $textimate = new \stdClass();
    if (!empty($form_state->getValue('animation_data'))) {
      $effects = [];
      foreach ($form_state->getValue('animation_data') as $index => $effect) {
        $effects[$index] = [
          'name'        => $effect['name'],
          'delay_scale' => $effect['options_wrapper']['delay_scale'],
          'delay'       => $effect['options_wrapper']['delay'],
          'mode'        => $effect['options_wrapper']['mode'],
          'sync'        => $effect['options_wrapper']['mode'] == 'sync',
          'shuffle'     => $effect['options_wrapper']['mode'] == 'shuffle',
          'reverse'     => $effect['options_wrapper']['mode'] == 'reverse',
          'weight'      => $effect['weight'],
        ];
      }
      $textimate->animations = $effects;
    }
    else {
      $textimate->animations = $options['effects'] ?? [];
    }

    // Show an empty effect when adding a new textimate.
    if (empty($textimate->animations)) {
      $textimate->animations = [
        0 => [],
      ];
    }
    else {
      if (is_numeric($form_state->get('to_remove'))) {
        unset($textimate->animations[$form_state->get('to_remove')]);
        $form_state->set('num_effects', $form_state->get('num_effects') - 1);
        $form_state->set('to_remove', NULL);
      }

      // If the number of rows has been incremented add another row.
      if ($form_state->get('num_effects') > count($textimate->animations)) {
        $textimate->animations[] = [];
      }
    }

    $form['animation_data_wrapper'] = [
      '#tree'   => FALSE,
      '#prefix' => '<div class="clear-block" id="animation-data-wrapper">',
      '#suffix' => '</div>',
    ];
    $form['animation_data_wrapper']['animation_data'] = $this->getAnimationDataForm($textimate, $config);

    // There are two functions attached to the more button:
    // The submit function will be called first; increments number of rows.
    // The callback function will then return the rendered rows.
    $form['effects_more'] = [
      '#name'       => 'effects_more',
      '#type'       => 'submit',
      '#value'      => $this->t('Add animation'),
      '#attributes' => [
        'class' => ['add-effect'],
        'title' => $this->t('Click here to add more effects.'),
      ],
      '#submit'     => [[$this, 'ajaxFormSubmit']],
      '#ajax'       => [
        'callback' => [$this, 'ajaxFormCallback'],
        'progress' => [
          'type'    => 'throbber',
          'message' => NULL,
        ],
        'effect'   => 'fade',
      ],
    ];

    // Vertical tabs for settings.
    $form['settings'] = [
      '#type'    => 'vertical_tabs',
      '#parents' => ['settings'],
      '#tree'    => TRUE,
    ];

    // Textimate main options.
    $form['options'] = [
      '#type'       => 'details',
      '#title'      => $this->t('Textimate settings'),
      '#attributes' => ['class' => ['details--settings', 'b-tooltip']],
      '#group'      => 'settings',
      '#open'       => TRUE,
    ];

    // Set whether to automatically start animating.
    $form['options']['auto_start'] = [
      '#title'         => $this->t('Auto start'),
      '#type'          => 'checkbox',
      '#default_value' => $options['autoStart'] ?? $config->get('options.autoStart'),
      '#description'   => $this->t('Set whether to automatically start animating.'),
      '#attributes'    => ['class' => ['textimate-auto-start']],
    ];

    // Enable animation looping.
    $form['options']['loop'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Loop'),
      '#default_value' => $options['loop'] ?? $config->get('options.loop'),
      '#description'   => $this->t("If enabled, element will repeat the animation."),
      '#attributes'    => ['class' => ['textimate-loop']],
    ];

    // Sets the initial delay before starting the animation.
    $form['options']['initial_delay'] = [
      '#title'         => $this->t('Initial delay'),
      '#type'          => 'textfield',
      '#size'          => 9,
      '#maxlength'     => 9,
      '#default_value' => $options['initialDelay'] ?? $config->get('options.initialDelay'),
      '#description'   => $this->t('Sets the initial delay before starting the animation.'),
      '#attributes'    => ['class' => ['textimate-initial-delay']],
      '#field_suffix'  => $ms_unit_label,
    ];

    // Sets the minimum display time for each text before it is replaced.
    $form['options']['min_display_time'] = [
      '#title'         => $this->t('Min display time'),
      '#type'          => 'textfield',
      '#size'          => 9,
      '#maxlength'     => 9,
      '#default_value' => $options['minDisplayTime'] ?? $config->get('options.minDisplayTime'),
      '#description'   => $this->t('Sets the minimum display time for each effect before it is replaced.'),
      '#attributes'    => ['class' => ['textimate-display-time']],
      '#field_suffix'  => $ms_unit_label,
    ];

    // Textimate advanced options.
    $form['advanced'] = [
      '#type'       => 'details',
      '#title'      => $this->t('Advanced options'),
      '#attributes' => ['class' => ['details--advanced', 'b-tooltip']],
      '#group'      => 'settings',
    ];

    // The splitting type for textimate effects.
    $form['advanced']['split'] = [
      '#title'         => $this->t('Type'),
      '#type'          => 'select',
      '#options'       => textimate_type_options(),
      '#default_value' => $options['split'] ?? $config->get('options.split'),
      '#description'   => $this->t('Select the effect type by words, characters or lines animation.'),
      '#attributes'    => ['class' => ['textimate-split']],
    ];

    // The javascript event for textimate effects.
    $form['advanced']['event'] = [
      '#title'         => $this->t('Event') . $experimental_label,
      '#type'          => 'select',
      '#options'       => animatecss_event_options(),
      '#default_value' => $options['event'] ?? $config->get('options.event'),
      '#description'   => $this->t('Select the javascript event to show animation.'),
      '#attributes'    => ['class' => ['textimate-event']],
    ];

    // Textimate administration tab.
    $form['administration'] = [
      '#type'       => 'details',
      '#title'      => $this->t('Administration'),
      '#attributes' => ['class' => ['details--administration', 'b-tooltip']],
      '#group'      => 'settings',
    ];

    // Enabled status for this textimate effects.
    $form['administration']['status'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Enabled'),
      '#description'   => $this->t('Effects will appear on pages that have this selector.'),
      '#default_value' => $status ?? TRUE,
    ];

    // The comment for describe animate settings and usage in website.
    $form['administration']['help'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Explanation guidelines'),
      '#default_value' => $comment ?? '',
      '#description'   => $this->t('Describe this animate settings and usage in your website. The text will be displayed only on this form as comment for administration section.'),
      '#rows'          => 3,
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#value'       => $this->t('Save'),
      '#button_type' => 'primary',
      '#submit'      => [[$this, 'submitForm']],
    ];

    if ($tid != 0) {
      // Add a 'Remove' button for effects form.
      $form['actions']['delete'] = [
        '#type'       => 'link',
        '#title'      => $this->t('Delete'),
        '#url'        => Url::fromRoute('textimate.delete', ['tid' => $tid]),
        '#attributes' => [
          'class' => [
            'action-link',
            'action-link--danger',
            'action-link--icon-trash',
          ],
        ],
      ];

      // Redirect to list for submit handler on edit form.
      $form['actions']['submit']['#submit'] = ['::submitForm', '::overview'];
    }
    else {
      // Add a 'Save and go to list' button for add form.
      $form['actions']['overview'] = [
        '#type'   => 'submit',
        '#value'  => $this->t('Save and go to list'),
        '#submit' => array_merge($form['actions']['submit']['#submit'], ['::overview']),
        '#weight' => 20,
      ];
    }

    // Attach textimate form library.
    $form['#attached']['library'][] = 'textimate/textimate.form';

    return $form;
  }

  /**
   * Ajax callback for the add tab and remove tab buttons.
   *
   * Returns the table rows.
   */
  public function ajaxFormCallback(array &$form, FormStateInterface $form_state) {
    // Clear selector validate error message via ajax.
    if ($form_state->getErrors()) {
      $this->messenger()->deleteAll();
    }

    // Instantiate an AjaxResponse Object to return.
    $ajax_response = new AjaxResponse();
    $ajax_response->addCommand(new HtmlCommand('#animation-data-wrapper', $form['animation_data_wrapper']['animation_data']));

    return $ajax_response;
  }

  /**
   * Submit handler for the 'Add Effect' and 'Remove' buttons.
   *
   * Removes a row or increments the number of rows depending on action.
   */
  public function ajaxFormSubmit(array &$form, FormStateInterface $form_state) {
    if ($form_state->getTriggeringElement()['#name'] === 'effects_more') {
      $form_state->set('num_effects', count($form_state->getValue('animation_data')) + 1);
      $form_state->setRebuild(TRUE);
    }
    elseif (is_numeric($form_state->getTriggeringElement()['#row_number'])) {
      $form_state->set('to_remove', $form_state->getTriggeringElement()['#row_number']);
      $form_state->setRebuild(TRUE);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $tid      = $form_state->getValue('textimate_id');
    $is_new   = $tid == 0;
    $selector = trim($form_state->getValue('selector'));

    if ($is_new) {
      if ($this->textimateManager->isTextimate($selector)) {
        $form_state->setErrorByName('selector', $this->t('This selector is already exists.'));
      }
    }
    else {
      if ($this->textimateManager->findById($tid)) {
        $textimate = $this->textimateManager->findById($tid);

        if ($selector != $textimate['selector'] && $this->textimateManager->isTextimate($selector)) {
          $form_state->setErrorByName('selector', $this->t('This selector is already added.'));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values    = $form_state->getValues();
    $variables = [];

    $tid      = $values['textimate_id'];
    $label    = trim($values['label']);
    $selector = trim($values['selector']);
    $comment  = trim($values['help']);
    $status   = $values['status'];

    // Provide a label from selector if was empty.
    if (empty($label)) {
      $label = ucfirst(trim(preg_replace("/[^a-zA-Z0-9]+/", " ", $selector)));
    }

    // Animation effect and options.
    foreach ($values['animation_data'] as $animation) {
      $variables['effects'][] = [
        'name'       => $animation['name'],
        'delayScale' => $animation['options_wrapper']['delay_scale'],
        'delay'      => $animation['options_wrapper']['delay'],
        'sequence'   => $animation['options_wrapper']['mode'] == 'sequence',
        'sync'       => $animation['options_wrapper']['mode'] == 'sync',
        'reverse'    => $animation['options_wrapper']['mode'] == 'reverse',
        'shuffle'    => $animation['options_wrapper']['mode'] == 'shuffle',
        'weight'     => $animation['weight'],
      ];
    }

    // Textimate main settings.
    $variables['loop']           = $values['loop'];
    $variables['minDisplayTime'] = $values['min_display_time'];
    $variables['initialDelay']   = $values['initial_delay'];
    $variables['autoStart']      = $values['auto_start'];

    // Advanced options data.
    $variables['split'] = $values['split'];
    $variables['event'] = $values['event'];

    // Serialize options variables.
    $options = serialize($variables);

    // The Unix timestamp when the textimate was most recently saved.
    $changed = $this->time->getCurrentTime();

    // Textimate save.
    $this->textimateManager->addTextimate($tid, $selector, $label, $comment, $changed, $status, $options);
    $this->messenger()
      ->addStatus($this->t('The selector %selector has been added.', ['%selector' => $selector]));

    // Flush caches so the updated config can be checked.
    drupal_flush_all_caches();
  }

  /**
   * Submit handler for removing textimate.
   *
   * @param array[] $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function remove(array &$form, FormStateInterface $form_state) {
    $tid = $form_state->getValue('textimate_id');
    $form_state->setRedirect('textimate.delete', ['tid' => $tid]);
  }

  /**
   * Form submission handler for the 'overview' action.
   *
   * @param array[] $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function overview(array $form, FormStateInterface $form_state): void {
    $form_state->setRedirect('textimate.admin');
  }

  /**
   * Returns animation data form.
   */
  private function getAnimationDataForm($textimate, $config) {
    $animation_data = [
      '#type'       => 'table',
      '#header'     => [
        ['data' => $this->t('Animation name')],
        ['data' => $this->t('Weight')],
        ['data' => $this->t('Preview')],
        ['data' => $this->t('Options')],
        ['data' => $this->t('Operations')],
      ],
      '#empty'      => $this->t('There are no effects yet.'),
      '#tabledrag'  => [
        [
          'action'       => 'order',
          'relationship' => 'sibling',
          'group'        => 'effects-order-weight',
        ],
      ],
      '#attributes' => ['class' => ['text-animate']],
    ];

    foreach ($textimate->animations as $index => $animation) {
      $mode = _textimate_check_mode($animation);

      $animation['delta']       = $index;
      $animation['name']        = $animation['name'] ?? $config->get('options.effects.name');
      $animation['delay_scale'] = $animation['delayScale'] ?? $config->get('options.effects.delayScale');
      $animation['delay']       = $animation['delay'] ?? $config->get('options.effects.delay');
      $animation['mode']        = !empty($mode) ? $mode : $config->get('options.mode');
      $animation['summary']     = $this->optionsSummary($animation);
      $animation['weight']      = $animation['weight'] ?? $index;

      $animation_data[$index] = $this->getRow($index, $animation);
    }

    return $animation_data;
  }

  /**
   * Builds and returns instance row.
   */
  private function getRow($row_number, $animation = NULL) {
    if ($animation === NULL) {
      $animation = [];
    }

    $row = [];
    // TableDrag: Mark the table row as draggable.
    $row['#attributes']['class'][] = 'draggable';
    // TableDrag: Sort the table row according to its configured weight.
    $row['#weight'] = $animation['weight'] ?? 0;

    // The animation effect name to use in.
    $row['name'] = [
      '#title'         => $this->t('Animation name'),
      '#title_display' => 'invisible',
      '#type'          => 'select',
      '#options'       => animatecss_animation_options(),
      '#default_value' => $animation['name'],
      '#attributes'    => ['class' => ['textimate-effect']],
    ];

    // TableDrag: Weight column element.
    $row['weight'] = [
      '#type'          => 'weight',
      '#title'         => $this->t('Weight'),
      '#title_display' => 'invisible',
      '#default_value' => $animation['weight'] ?? 0,
      '#attributes'    => ['class' => ['effects-order-weight']],
    ];

    // Display a summary of the current effect settings, and (if the
    // summary is not empty) a button to edit them.
    $row['preview'] = [
      '#markup' => 'Textimate Now!',
      '#prefix' => '<p class="textimate-now" style="font-size: 30px;">',
      '#suffix' => '</p>',
    ];

    // Settings wrapper.
    $row['settings_wrapper'] = [
      '#type'       => 'container',
      '#attributes' => ['class' => ['textimate-settings', 'container-inline']],
    ];

    // Edit settings cog icon button.
    $row['settings_wrapper']['settings_edit'] = [
      '#type'       => 'image_button',
      '#name'       => 'settings_edit',
      '#src'        => 'core/misc/icons/787878/cog.svg',
      '#op'         => 'edit',
      '#prefix'     => '<div class="textimate-settings-edit-wrapper">',
      '#suffix'     => '</div>',
      '#attributes' => [
        'class' => ['textimate-edit'],
        'alt'   => $this->t('Edit'),
      ],
    ];

    // Effect settings summary.
    $row['settings_wrapper']['settings_summary'] = [
      '#type'            => 'inline_template',
      '#template'        => '<div class="textimate-summary field-plugin-summary">{{ summary|safe_join("<br />") }}</div>',
      '#context'         => ['summary' => $animation['summary']],
      '#cell_attributes' => ['class' => ['field-plugin-summary-cell']],
    ];

    // Effect options wrapper.
    $row['options_wrapper'] = [
      '#type'       => 'container',
      '#attributes' => ['class' => ['textimate-options', 'container-inline']],
    ];

    // The delay factor applied to each consecutive character.
    $row['options_wrapper']['delay_scale'] = [
      '#title'         => $this->t('Delay scale'),
      '#type'          => 'textfield',
      '#size'          => 3,
      '#maxlength'     => 6,
      '#default_value' => $animation['delay_scale'],
      '#attributes'    => ['class' => ['textimate-delay-scale']],
    ];

    // Set the delay between each character.
    $row['options_wrapper']['delay'] = [
      '#title'         => $this->t('Delay'),
      '#type'          => 'textfield',
      '#size'          => 3,
      '#maxlength'     => 6,
      '#default_value' => $animation['delay'],
      '#attributes'    => ['class' => ['textimate-delay']],
    ];

    // The animation character modes by:
    // sequence, randomize and reverse sync.
    $row['options_wrapper']['mode'] = [
      '#title'         => $this->t('Mode'),
      '#type'          => 'select',
      '#options'       => textimate_mode_options(),
      '#default_value' => $animation['mode'],
      '#attributes'    => ['class' => ['textimate-mode']],
    ];

    // Effect option operations container.
    $row['options_operations'] = [
      '#type'       => 'container',
      '#attributes' => ['class' => ['textimate-options', 'container-inline']],
    ];

    // Effect update button.
    $row['options_operations']['settings_update'] = [
      '#type'        => 'button',
      '#name'        => 'update-' . $row_number,
      '#value'       => $this->t('Update'),
      '#button_type' => 'primary',
      '#attributes'  => ['class' => ['textimate-update']],
    ];

    // Effect cancel button.
    $row['options_operations']['settings_cancel'] = [
      '#type'       => 'button',
      '#name'       => 'cancel-' . $row_number,
      '#value'      => $this->t('Cancel'),
      '#attributes' => ['class' => ['textimate-cancel']],
    ];

    // There are two functions attached to the remove button.
    // The submit function will be called first and used to remove selected row.
    // The callback function will then return the rendered rows.
    $row['operations'] = [
      '#row_number' => $row_number,
      // We need this - the call to getTriggeringElement when clicking the
      // remove button won't work without it.
      '#name'       => 'row-' . $row_number,
      '#type'       => 'submit',
      '#value'      => $this->t('Remove'),
      '#attributes' => [
        'class' => ['textimate-delete'],
        'title' => $this->t('Click here to delete this effect.'),
      ],
      '#submit'     => [[$this, 'ajaxFormSubmit']],
      '#ajax'       => [
        'callback' => [$this, 'ajaxFormCallback'],
        'progress' => [
          'type'    => 'throbber',
          'message' => NULL,
        ],
        'effect'   => 'fade',
      ],
      '#button_type' => 'danger',
    ];

    return $row;
  }

  /**
   * {@inheritdoc}
   */
  public function optionsSummary($animation) {
    $delay = empty($animation['delay']) ? 0 : $animation['delay'];
    $modes = textimate_mode_options();
    return [
      $this->t('Delay Scale: @title', ['@title' => $animation['delay_scale']]),
      $this->t('Delay: @title', ['@title' => $delay]),
      $this->t('Mode: @title', ['@title' => $modes[$animation['mode']]]),
    ];
  }

}
