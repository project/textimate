<?php

namespace Drupal\textimate\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\textimate\TextimateManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to duplicate textimate.
 *
 * @internal
 */
class TextimateDuplicate extends FormBase {

  /**
   * The Animate selector.
   *
   * @var int
   */
  protected $tid;

  /**
   * The Animate selector manager.
   *
   * @var \Drupal\textimate\TextimateManagerInterface
   */
  protected $textimateManager;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new textimateDelete object.
   *
   * @param \Drupal\textimate\TextimateManagerInterface $textimate_manager
   *   The Animate selector manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(TextimateManagerInterface $textimate_manager, TimeInterface $time) {
    $this->textimateManager = $textimate_manager;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('textimate.effect_manager'),
      $container->get('datetime.time'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'textimate_duplicate_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param int $tid
   *   The Textimate record ID to remove.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $tid = 0) {
    $form['textimate_id'] = [
      '#type'  => 'value',
      '#value' => $tid,
    ];

    // New selector to duplicate effect.
    $form['selector'] = [
      '#title'         => $this->t('Selector'),
      '#type'          => 'textfield',
      '#required'      => TRUE,
      '#size'          => 64,
      '#maxlength'     => 255,
      '#default_value' => '',
      '#description'   => $this->t('Here, you can use HTML tag, class with dot(.) and ID with hash(#) prefix. Be sure your selector has plain text content. e.g. ".page-title" or ".block-title".'),
      '#placeholder'   => $this->t('Enter valid selector'),
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#value' => $this->t('Duplicate'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $tid      = $form_state->getValue('textimate_id');
    $is_new   = $tid == 0;
    $selector = trim($form_state->getValue('selector'));

    if ($is_new) {
      if ($this->textimateManager->isTextimate($selector)) {
        $form_state->setErrorByName('selector', $this->t('This selector is already exists.'));
      }
    }
    else {
      if ($this->textimateManager->findById($tid)) {
        $textimate = $this->textimateManager->findById($tid);

        if ($selector != $textimate['selector'] && $this->textimateManager->isTextimate($selector)) {
          $form_state->setErrorByName('selector', $this->t('This selector is already added.'));
        }
      }
    }
  }

  /**
   * Form submission handler for the 'duplicate' action.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   A reference to a keyed array containing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $tid      = $values['textimate_id'];
    $selector = trim($values['selector']);
    $label    = ucfirst(trim(preg_replace("/[^a-zA-Z0-9]+/", " ", $selector)));
    $status   = 1;

    $textimate = $this->textimateManager->findById($tid);
    $comment   = $textimate['comment'];
    $options   = $textimate['options'];

    // The Unix timestamp when the textimate was most recently saved.
    $changed = $this->time->getCurrentTime();

    // Textimate save.
    $new_tid = $this->textimateManager->addTextimate(0, $selector, $label, $comment, $changed, $status, $options);
    $this->messenger()
      ->addStatus($this->t('The selector %selector has been duplicated.', ['%selector' => $selector]));

    // Flush caches so the updated config can be checked.
    drupal_flush_all_caches();

    // Redirect to duplicated effect edit form.
    $form_state->setRedirect('textimate.edit', ['tid' => $new_tid]);
  }

}
